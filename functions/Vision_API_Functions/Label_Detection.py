import io
from google.cloud import vision


def detect_labels(path):
    """Detects labels in the image file and returns a list of tuples, where each tuple contains the label description and the percentage score."""
    # Create a Vision client.
    client = vision.ImageAnnotatorClient()

    # Read the image file.
    with io.open(path, "rb") as image_file:
        content = image_file.read()

    # Create an Image object from the image content.
    image = vision.Image(content=content)

    # Detect labels in the image.
    response = client.label_detection(image=image)
    labels = response.label_annotations

    # Create a list of tuples, where each tuple contains the label description and the percentage score.
    labeled_scores = []
    for label in labels:
        labeled_scores.append((label.description, "%.2f%%" % (label.score * 100)))

    # Return the list of tuples.
    return labeled_scores


labeled_scores = detect_labels("Resources/label-detection.jpg")
print("The labels and their percentages are:")
for label, score in labeled_scores:
    print("Label:", label, "Score:", score)


detect_labels("functions/Resources/label-detection.jpg.jpg")
